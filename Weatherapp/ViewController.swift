//
//  ViewController.swift
//  Weatherapp
//
//  Created by Click Labs on 1/21/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITextFieldDelegate {

  @IBOutlet weak var wallpaper: UIImageView!
  @IBOutlet weak var cityName: UITextField!
  @IBOutlet weak var resultLabel: UILabel!
  
  
  @IBAction func displayButton(sender: AnyObject) {  //
    self.view.endEditing(true)
  
    var urlString = "http://www.weather-forecast.com/locations/" + cityName.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest"
    var url = NSURL(string : urlString)
    let task = NSURLSession.sharedSession().dataTaskWithURL(url!)  { (data , response ,error) in
    var urlContent = NSString(data: data ,encoding: NSUTF8StringEncoding)
      
      if NSString(string:urlContent!).containsString("<span class=\"phrase\">") {
      
        var contentArray   = urlContent!.componentsSeparatedByString("<span class=\"phrase\">")
        var newContentArray = contentArray[1].componentsSeparatedByString("</span>")
        dispatch_async(dispatch_get_main_queue()){
           var news = newContentArray[0].stringByReplacingOccurrencesOfString("&deg;", withString:"º")
           self.wallpaper.hidden = true
           self.resultLabel.text = news
        }
      }
      
      else { dispatch_async(dispatch_get_main_queue()){
              self.resultLabel.text = "Couldnt find city"
             }
      }
    }
    
      task.resume()
   
   }
  
  @IBAction func resetButton(sender: AnyObject) { //reset button
    wallpaper.hidden = false
    self.resultLabel.text = ""
    cityName.text = ""
  }
  
  

  func textFieldShouldReturn(textField: UITextField) -> Bool { // func to hide keyboard on pressing return button
    cityName.resignFirstResponder()
    return true
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) { // func to hide keyboard on touching outside
    self.view.endEditing(true)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

